# Docker containers for GPAW CI

## Building and pushing images

Use the makefile to build, push, and tag/publish images.

If using podman, define

    export BUILDAH_FORMAT=docker

To build e.g. the latest "main" docker and tag it as "staging":

    make build-main-latest

Login using GitLab Personal Access Token in order to be able to push:

    docker login registry.gitlab.com

To push the "staging" docker:

    make push-main-latest

To tag the current "staging" docker for production and push it:

    make publish-main-latest

The CAMD runner does not automatically download the updated
images for security reasons.  You need to ssh into it
and pull the images separately.
