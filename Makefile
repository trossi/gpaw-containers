IMAGE_PATH = registry.gitlab.com/gpaw/gpaw-ci-containers

build-main-oldest: gpaw-setups-0.9.20000.tar.gz gpaw-basis-pvalence-0.9.20000.tar.gz
	cp $^ main/  # "$^" means all the prerequisites
	docker build \
		--build-arg BASE_IMAGE=docker.io/python:3.9-slim-bullseye \
        --build-arg PYPACKAGES="numpy==1.19.5 scipy==1.6.0" \
		-t ${IMAGE_PATH}/main:oldest-staging \
		main/

build-main-latest: gpaw-setups-0.9.20000.tar.gz gpaw-basis-pvalence-0.9.20000.tar.gz
	cp $^ main/
	docker build \
		--build-arg BASE_IMAGE=docker.io/python:3.11-slim-bullseye \
		--build-arg PYPACKAGES="numpy scipy" \
		-t ${IMAGE_PATH}/main:latest-staging \
		main/

build-cuda-11: gpaw-setups-0.9.20000.tar.gz gpaw-basis-pvalence-0.9.20000.tar.gz
	cp $^ cuda/
	docker build \
		--build-arg BASE_IMAGE=docker.io/python:3.11-slim-bullseye \
		--build-arg CUDA_VERSION=11.6.2 \
		--build-arg HIP_VERSION=5.3.3 \
		--build-arg PYPACKAGES="numpy scipy" \
		-t ${IMAGE_PATH}/cuda:11-staging \
		cuda/

gpaw-%.tar.gz:
	wget https://wiki.fysik.dtu.dk/gpaw-files/$@

push-main-oldest:
	docker push ${IMAGE_PATH}/main:oldest-staging

push-main-latest:
	docker push ${IMAGE_PATH}/main:latest-staging

push-cuda-11:
	docker push ${IMAGE_PATH}/cuda:11-staging

publish-main-oldest:
	docker image tag ${IMAGE_PATH}/main:oldest-staging ${IMAGE_PATH}/main:oldest
	docker push ${IMAGE_PATH}/main:oldest

publish-main-latest:
	docker image tag ${IMAGE_PATH}/main:latest-staging ${IMAGE_PATH}/main:latest
	docker push ${IMAGE_PATH}/main:latest

publish-cuda-11:
	docker image tag ${IMAGE_PATH}/cuda:11-staging ${IMAGE_PATH}/cuda:11
	docker push ${IMAGE_PATH}/cuda:11

shell-main-oldest:
	docker run --name gpaw-ci-main-oldest --rm -i -t ${IMAGE_PATH}/main:oldest-staging bash

shell-main-latest:
	docker run --name gpaw-ci-main-latest --rm -i -t ${IMAGE_PATH}/main:latest-staging bash
